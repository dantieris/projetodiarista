package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static final String SUPER_DIARISTA_PU_DANTI = "SuperDiaristasPU_DANTI";
    private static final String SUPER_DIARISTA_PU_SENAC = "SuperDiaristasPU_SENAC";
    
    private static final EntityManagerFactory EMF = 
            Persistence.createEntityManagerFactory(SUPER_DIARISTA_PU_DANTI);
    
    public static EntityManager getEntityManager() {
        return EMF.createEntityManager();
    }
    
    public static void close(){
        EMF.close();
    }
}
