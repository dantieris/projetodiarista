package dao;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import model.Diarista;
import util.JPAUtil;

public class DiaristaDAO {    
    
    public DiaristaDAO() {}
    
    public void salvar(Diarista diarista) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        if (diarista.getId() == null) {
            System.out.println("Persist: " + diarista);
            em.persist(diarista);
        } else {
            System.out.println("Merge: " + diarista);
            em.merge(diarista);
        }
        em.getTransaction().commit();
        em.close();
    }
    
    public List<Diarista> buscarPorTurnoEData(String turno, Date data) {
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT d FROM Diarista d WHERE d.turno = :turno and d.data = :data");
        
        query.setParameter("turno", turno);
        query.setParameter("data", data);
        
        List<Diarista> diaristas = query.getResultList();
        em.close();
        return (diaristas);
    }

    public Diarista buscarLogin(String usuario, String senha) {
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT d FROM Diarista d WHERE d.usuario = :usuario and d.senha = :senha");
        
        query.setParameter("usuario", usuario);
        query.setParameter("senha", senha);
        
        Diarista diarista = null;
        try {
            diarista = (Diarista) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            return diarista;
        }
    }
}
