package dao;

import model.Contratante;
import model.Diarista;

public class UsuarioDAO {

    public UsuarioDAO() {
    }

    public Object buscarLogin(String usuario, String senha) {
        DiaristaDAO diaristaDAO = new DiaristaDAO();
        Diarista diarista = diaristaDAO.buscarLogin(usuario, senha);
        
        if (diarista != null) {
            return diarista;
        }
        
        ContratanteDAO contratanteDAO = new ContratanteDAO();
        Contratante contratante = contratanteDAO.buscarLogin(usuario, senha);
        
        if (contratante != null) {
            return contratante;
        }
        return null;
    }
    
    
}
