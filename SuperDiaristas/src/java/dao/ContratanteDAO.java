package dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Contratante;
import util.JPAUtil;

public class ContratanteDAO {

    public ContratanteDAO() {}
    
    public void salvar(Contratante contratante) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        if (contratante.getId() == null) {
            System.out.println("Persist: " + contratante);
            em.persist(contratante);
        } else {
            System.out.println("Merge: " + contratante);
            em.merge(contratante);
        }
        em.getTransaction().commit();
        em.close();
    }

    public Contratante buscarLogin(String usuario, String senha) {
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT c FROM Contratante c WHERE c.usuario = :usuario and c.senha = :senha");
        
        query.setParameter("usuario", usuario);
        query.setParameter("senha", senha);
        
        Contratante contratante = null;
        try {
            contratante = (Contratante) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
            return contratante;
        }
    }
    
    
}
