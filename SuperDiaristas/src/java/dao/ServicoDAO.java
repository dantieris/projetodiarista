package dao;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Servico;
import util.JPAUtil;

public class ServicoDAO {

    public ServicoDAO() {}
    
    public void salvar(Servico servico) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        if (servico.getId() == null) {
            em.persist(servico);
        } else {
            em.merge(servico);
        }
        em.getTransaction().commit();
        em.close();
    }

    // Métodos relacionado a solicitações.
    
    /**
     * Busca os serviços que estão com o estado solicitado.
     * 
     * @param idContratante
     * @param idDiarista
     * @return 
     */
    public List<Servico> buscarSolicitacoes(long idContratante, long idDiarista) {
        if (idContratante > 0 && idDiarista == 0) {
            return buscarSolicitacoesContratante(idContratante, Servico.ESTADO_SOLICITADO);
        }
        if (idDiarista > 0 && idContratante == 0) {
            return buscarSolicitasoesDiarista(idDiarista, Servico.ESTADO_SOLICITADO);
        }
        return buscarPorEstado(idContratante, idDiarista, Servico.ESTADO_SOLICITADO);
    }
    
    /**
     * Busca o serviços de acordo com o id da contratante e o estado do serviço.
     * @param idContratante
     * @param estado
     * @return 
     */
    private List<Servico> buscarSolicitacoesContratante(long idContratante, int estado) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.estado = :estado "
                      +"AND s.contratante.id = :idContratante "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("estado", estado);
        query.setParameter("idContratante", idContratante);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * Busca os serviços de acordo com o id da diarista e o estado do serviço.
     * 
     * @param idDiarista
     * @param estado
     * @return 
     */
    private List<Servico> buscarSolicitasoesDiarista(long idDiarista, int estado) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.estado = :estado "
                      +"AND s.diarista.id = :idDiarista "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("estado", estado);
        query.setParameter("idDiarista", idDiarista);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * Busca os serviços de acordo com os ids da contratante e diarista e o estado do serviço.
     * 
     * @param idContratante
     * @param idDiarista
     * @param estado
     * @return 
     */
    private List<Servico> buscarPorEstado(long idContratante, long idDiarista, int estado) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.estado = :estado "
                      +"AND s.contratante.id = :idContratante "
                      +"AND S.diarista.id = :idDiarista "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("estado", estado);
        query.setParameter("idContratante", idContratante);
        query.setParameter("idDiarista", idDiarista);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    public List<Servico> buscarSolicitacoes(long idContratante, long idDiarista, Date data, String turno) {
        if (idContratante > 0 && idDiarista == 0) {
            return buscarSolicitacoesContratante(idContratante, Servico.ESTADO_SOLICITADO, data, turno);
        }
        if (idDiarista > 0 && idContratante == 0) {
            return buscarSolicitasoesDiarista(idDiarista, Servico.ESTADO_SOLICITADO, data, turno);
        }
        return buscarPorEstado(idContratante, idDiarista, Servico.ESTADO_SOLICITADO, data, turno);
    }
    
    /**
     * Busca o serviços de acordo com o id da contratante e o estado do serviço.
     * 
     * @param idDiarista
     * @param estado
     * @param data
     * @param turno
     * @return 
     */
    private List<Servico> buscarSolicitacoesContratante(long idContratante, int estado, Date data, String turno) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.estado = :estado "
                      +"AND s.contratante.id = :idContratante "
                      +"AND s.data = :data "
                      +"AND s.turno = :turno "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("estado", estado);
        query.setParameter("idContratante", idContratante);
        query.setParameter("data", data);
        query.setParameter("turno", turno);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * Busca os serviços de acordo com o id da diarista e o estado do serviço.
     * 
     * @param idDiarista
     * @param estado
     * @param data
     * @param turno
     * @return 
     */
    private List<Servico> buscarSolicitasoesDiarista(long idDiarista, int estado, Date data, String turno) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.estado = :estado "
                      +"AND s.diarista.id = :idDiarista "
                      +"AND s.data = :data "
                      +"AND s.turno = :turno "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("estado", estado);
        query.setParameter("idDiarista", idDiarista);
        query.setParameter("data", data);
        query.setParameter("turno", turno);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * Busca os serviços de acordo com os ids da contratante e diarista e o estado do serviço.
     * 
     * @param idDiarista
     * @param estado
     * @param data
     * @param turno
     * @return 
     */
    private List<Servico> buscarPorEstado(long idContratante, long idDiarista, int estado, Date data, String turno) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.estado = :estado "
                      +"AND s.contratante.id = :idContratante "
                      +"AND S.diarista.id = :idDiarista "
                      +"AND s.turno = :turno "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("estado", estado);
        query.setParameter("idContratante", idContratante);
        query.setParameter("idDiarista", idDiarista);
        query.setParameter("data", data);
        query.setParameter("turno", turno);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    // Métodos relacionados ao histórico
    
    /**
     * 
     * @param idContratante
     * @param idDiarista
     * @return 
     */
    public List<Servico> buscarHistoricos(long idContratante, long idDiarista) {
        if (idContratante > 0 && idDiarista == 0) {
            return buscarHistoricoContratante(idContratante);
        }
        if (idDiarista > 0 && idContratante == 0) {
            return buscarHistoricoDiarista(idDiarista);
        }
        return buscar(idContratante, idDiarista);
    }
    
    /**
     * 
     * @param idContratante
     * @return 
     */
    private List<Servico> buscarHistoricoContratante(long idContratante) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.contratante.id = :idContratante "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("idContratante", idContratante);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * 
     * @param idDiarista
     * @return 
     */
    private List<Servico> buscarHistoricoDiarista(long idDiarista) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.diarista.id = :idDiarista "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("idDiarista", idDiarista);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * 
     * @param idContratante
     * @param idDiarista
     * @return 
     */
    private List<Servico> buscar(long idContratante, long idDiarista) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.contratante.id = :idContratante "
                      +"AND S.diarista.id = :idDiarista "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("idContratante", idContratante);
        query.setParameter("idDiarista", idDiarista);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * 
     * @param idContratante
     * @param idDiarista
     * @return 
     */
    public List<Servico> buscarHistoricos(long idContratante, long idDiarista, Date data, String turno) {
        if (idContratante > 0 && idDiarista == 0) {
            return buscarHistoricoContratante(idContratante, data, turno);
        }
        if (idDiarista > 0 && idContratante == 0) {
            return buscarHistoricoDiarista(idDiarista, data, turno);
        }
        return buscar(idContratante, idDiarista, data, turno);
    }
    
    /**
     * 
     * @param idContratante
     * @return 
     */
    private List<Servico> buscarHistoricoContratante(long idContratante, Date data, String turno) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.contratante.id = :idContratante "
                      +"AND s.data = :data "
                      +"AND s.turno = :turno "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("idContratante", idContratante);
        query.setParameter("data", data);
        query.setParameter("turno", turno);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * 
     * @param idDiarista
     * @return 
     */
    private List<Servico> buscarHistoricoDiarista(long idDiarista, Date data, String turno) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.diarista.id = :idDiarista "
                      +"AND s.data = :data "
                      +"AND s.turno = :turno "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("idDiarista", idDiarista);
        query.setParameter("data", data);
        query.setParameter("turno", turno);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
    
    /**
     * 
     * @param idContratante
     * @param idDiarista
     * @return 
     */
    private List<Servico> buscar(long idContratante, long idDiarista, Date data, String turno) {
        List<Servico> servicos;
        
        String jpql = "SELECT s FROM Servico s "
                      +"WHERE s.contratante.id = :idContratante "
                      +"AND S.diarista.id = :idDiarista "
                      +"AND s.data = :data "
                      +"AND s.turno = :turno "
                      +"ORDER BY s.id DESC";
        
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery(jpql);
        
        query.setParameter("idContratante", idContratante);
        query.setParameter("idDiarista", idDiarista);
        query.setParameter("data", data);
        query.setParameter("turno", turno);
        
        servicos = query.getResultList();
        em.close();
        
        return (servicos);
    }
}
