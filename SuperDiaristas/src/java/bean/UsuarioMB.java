package bean;

import dao.ContratanteDAO;
import dao.DiaristaDAO;
import dao.UsuarioDAO;
import java.io.Serializable;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.Contratante;
import model.Diarista;
import model.Log;
import ws.ClienteLogs;

@ManagedBean
@SessionScoped
public class UsuarioMB implements Serializable {

    // Usuário
    private String usuario;
    private String senha;
    
    // Diarista e Contratante
    private String nome;
    private String email;
    private String telefone;
    
    // Diarista
    private String turno;
    private Date data;
    
    // Usuário Logado
    private Object usuarioLogado;
    private Boolean usuarioLogadoContratante;
    private Boolean usuarioLogadoDiarista;
    
    // Ações de log.
    public static final String ACAO_LOG_LOGIN = "login";
    public static final String ACAO_LOG_LOGOUT = "logout";
    public static final String ACAO_LOG_CADASTRO = "cadastro";
    
    /*
    * Construtores.
    */
    
    public UsuarioMB() {
        turno = "";
        data  = null;
    }
    
    /*
    * Login.
    */
    
    public String login() {
        UsuarioDAO usuarioDAO = new UsuarioDAO();
        
        Object usuarioEncontrado = usuarioDAO.buscarLogin(usuario, senha);
        
        if (usuarioEncontrado == null) {
            long idUsuario = 0;
            String dados = "Tentativa de login com o usuário: " + usuario;
            adicionarLog(ACAO_LOG_LOGIN, idUsuario, dados);
            
            return ("login?faces-redirect=true");
        }
        usuarioLogado = usuarioEncontrado;
        
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("usuarioLogado", usuarioEncontrado);
        
        usuarioLogadoContratante();
        usuarioLogadoDiarista();
        
        String dados = "Login com sucesso do usuário: " + usuarioLogado;
        adicionarLog(ACAO_LOG_LOGIN, dados);
        
        return("index?faces-redirect=true");
    }
    
    /*
    * Logout.
    */
    
    public String logout() {
        String dados = "Logout com sucesso do usuário: " + usuarioLogado;
        adicionarLog(ACAO_LOG_LOGOUT, dados);
        
        usuarioLogado            = null;
        usuarioLogadoContratante = false;
        usuarioLogadoDiarista    = false;
        
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("usuarioLogado");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        
        return ("index?faces-redirect=true");
    }
    
    /*
    * Cadastros.
    */
    
    public String cadastrar() {
        if ("".equals(turno) || turno == null || data == null) {
            cadastrarContratante();            
            return ("index?faces-redirect=true");
        }
        cadastrarDiarista();
        return ("index?faces-redirect=true");
    }
    
    private void cadastrarContratante() {
        Contratante contratante = new Contratante(usuario, senha, nome, email, telefone);
        
        ContratanteDAO contratanteDAO = new ContratanteDAO();
        contratanteDAO.salvar(contratante);
        
        long idUsuario = 0;
        String dados = "Cadastro com sucesso do contratante: " + contratante;
        adicionarLog(ACAO_LOG_CADASTRO, idUsuario, dados);
    }

    private void cadastrarDiarista() {
        Diarista diarista = new Diarista(usuario, senha, nome, email, telefone, turno, data);
        
        DiaristaDAO diaristaDAO = new DiaristaDAO();
        diaristaDAO.salvar(diarista);
        
        long idUsuario = 0;
        String dados = "Cadastro com sucesso do diarista: " + diarista;
        adicionarLog(ACAO_LOG_CADASTRO, idUsuario, dados);
    }
    
    /*
    * Define usuário logado.
    */    
    public void usuarioLogadoContratante() {
        if (usuarioLogado == null) {
            usuarioLogadoContratante = false;
            return;
        }
        if (usuarioLogado instanceof Contratante) {
            usuarioLogadoContratante = true;
            return;
        }
        usuarioLogadoContratante = false;
    }
    
    public void usuarioLogadoDiarista() {
        if (usuarioLogado == null) {
            usuarioLogadoDiarista = false;
            return;
        }
        if (usuarioLogado instanceof Diarista) {
            usuarioLogadoDiarista = true;
            return;
        }
        usuarioLogadoDiarista = false;
    }
    
    /*
    * Adicionar logs.
    */
    
    protected void adicionarLog(String acao, long idUsuario, String dados) {
        Log log = new Log(acao, new Date(), idUsuario, dados);
        ClienteLogs cliente = new ClienteLogs();
        cliente.adicionarLog(log);
    }
    
    protected void adicionarLog(String acao, String dados) {
        long idUsuario = 0;
        
        if (usuarioLogadoContratante) {
            idUsuario = ((Contratante)usuarioLogado).getId();
        } else if (usuarioLogadoDiarista) {
            idUsuario = ((Diarista)usuarioLogado).getId();
        }
        
        Log log = new Log(acao, new Date(), idUsuario, dados);
        ClienteLogs cliente = new ClienteLogs();
        cliente.adicionarLog(log);
    }
    
    /*
    * Getters and Setters
    */

    public Boolean getUsuarioLogadoContratante() {
        return usuarioLogadoContratante;
    }

    public void setUsuarioLogadoContratante(Boolean usuarioLogadoContratante) {
        this.usuarioLogadoContratante = usuarioLogadoContratante;
    }

    public Boolean getUsuarioLogadoDiarista() {
        return usuarioLogadoDiarista;
    }

    public void setUsuarioLogadoDiarista(Boolean usuarioLogadoDiarista) {
        this.usuarioLogadoDiarista = usuarioLogadoDiarista;
    }

    public Object getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Object usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}