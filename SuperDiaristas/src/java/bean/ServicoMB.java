package bean;

import dao.DiaristaDAO;
import dao.ServicoDAO;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import model.Contratante;
import model.Diarista;
import model.Log;
import model.Servico;
import ws.ClienteLogs;

@ManagedBean
@SessionScoped
public class ServicoMB implements Serializable {
    
    // Tela Index.
    private String turno;
    private Date data;
    private List<Diarista> diaristas;
    
    private Boolean mostrarResultado;
    
    // Solicitações e Index
    private Contratante contratante;
    private Diarista diarista;
    
    // Tela solicitações
    private List<Servico> solicitacoes;
    
    // Tela historico
    private List<Servico> servicos;
    
    // Ações de log.
    public static final String ACAO_LOG_BUSCAR = "buscar_servico";
    
    public static final String ACAO_LOG_SOLICITAR = "solicitar_servico";
    public static final String ACAO_LOG_SOLICITACOES = "solicitacoes";
    public static final String ACAO_LOG_CANCELAR_SOLICITACAO = "cancelar_solicitacao";
    public static final String ACAO_LOG_ACEITAR_SOLICITACAO = "aceitar_solicitacao";
    
    public static final String ACAO_LOG_HISTORICO = "historicos";
    
    /*
    * Construtores e pós construtores.
    */
    
    public ServicoMB() {}
    
    @PostConstruct
    public void init() {
        data   = new Date();
        mostrarResultado = false;
        turno = "";
    }

    /*
    * Buscar serviços.
    */
    
    public void buscar() {
        carregarServicos();
    }
    
    private void carregarServicos() {
        DiaristaDAO diaristaDAO = new DiaristaDAO();
        diaristas = diaristaDAO.buscarPorTurnoEData(turno, data);
        
        adicionarLog(ACAO_LOG_BUSCAR, "Busca com os parâmetros: " 
                                    + "Data: " + data + ", Turno: " + turno 
                                    + "Resultado: " +  diaristas.toString());
        
        if (diaristas.size() > 0) {
            setMostrarResultado(true);
            return;
        }
        setMostrarResultado(false);
    }
    
    /*
    * Solicitar serviço.
    */
    
    public String solicitar(Diarista diarista) {
        if (!carregarContratanteLogado()) {
            adicionarLog(ACAO_LOG_SOLICITAR, "Tentativa de solicitar "
                            + "serviço com o diarísta: " + diarista.toString() 
                            + " mas usuário logado não é um contratante");
            
            return "login?faces-redirect=true";
        }
        salvarSolicitacao(diarista);
        limparCamposPesquisa();
        return ("solicitacao?faces-redirect=true");
    }
    
    private void salvarSolicitacao(Diarista diarista) {
        Servico servico = new Servico(contratante, diarista, Servico.ESTADO_SOLICITADO, diarista.getTurno(), diarista.getData());
        ServicoDAO servicoDAO = new ServicoDAO();
        servicoDAO.salvar(servico);
        
        adicionarLog(ACAO_LOG_SOLICITAR, "Serviço solicitado com sucesso: " + servico);
        
        carregarSolicitacoes(servicoDAO);
    }
    
    /*
    * Solicitações de serviços.
    */
    
    public String solicitacoes() {
        if (!carregarContratanteLogado() && !carregarDiaristaLogado()) {
            
            adicionarLog(ACAO_LOG_SOLICITACOES, 0, "Tentativa de acessar solicitações com usuário deslogado");
            
            return "login?faces-redirect=true";
        }
        carregarSolicitacoes(new ServicoDAO());
        limparCamposPesquisa();
        
        return ("solicitacao?faces-redirect=true");
    }
    
    private void carregarSolicitacoes(ServicoDAO servicoDAO) {
        if (diarista != null && contratante == null) {
            solicitacoes     = servicoDAO.buscarSolicitacoes(0, diarista.getId());
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_SOLICITACOES, diarista.getId(), "Solcitações "
                                + "carregados do diarista: " + diarista.toString() 
                                + " solicitações: " + solicitacoes.toString());
            
            return;
        }
        if (contratante != null & diarista == null) {
            solicitacoes     = servicoDAO.buscarSolicitacoes(contratante.getId(), 0);
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_SOLICITACOES, contratante.getId(), "Solcitações"
                                + " carregados do contratante: " + contratante.toString() 
                                + " solicitações: " + solicitacoes.toString());
        }
    }
    
    public void buscarSolicitacoes() {
        carregarSolicitacoes(new ServicoDAO(), data, turno);
    }
    
    private void carregarSolicitacoes(ServicoDAO servicoDAO, Date data, String turno) {
        if (diarista != null && contratante == null) {
            solicitacoes     = servicoDAO.buscarSolicitacoes(0, diarista.getId(), data, turno);
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_SOLICITACOES, diarista.getId(), 
                    "Buscar solicitações do diarista: " + diarista.toString() 
                    + " com os parâmetros: Data:" + data + ", Turno: " + turno 
                    + "Resultado: " + solicitacoes.toString());
            
            return;
        }
        if (contratante != null & diarista == null) {
            solicitacoes     = servicoDAO.buscarSolicitacoes(contratante.getId(), 0, data, turno);
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_SOLICITACOES, contratante.getId(), 
                    "Buscar solicitações do contratante: " + contratante.toString() 
                    + " com os parâmetros: Data:" + data + ", Turno: " + turno 
                    + "Resultado: " + solicitacoes.toString());
        }
    }
    
    public String aceitar(Servico solicitacao) {
        alterarEstadoServico(solicitacao,Servico.ESTADO_AGENDADO);
        adicionarLog(ACAO_LOG_ACEITAR_SOLICITACAO, "Solicitação aceita com sucesso: " + solicitacao.toString());
        return ("historico?faces-redirect=true");
    }
    
    public String cancelar(Servico solicitacao) {        
        alterarEstadoServico(solicitacao,Servico.ESTADO_CANCELADO);
        adicionarLog(ACAO_LOG_CANCELAR_SOLICITACAO, "Solicitação cancelada com sucesso: " + solicitacao.toString());
        return ("historico?faces-redirect=true");
    }
    
    private void alterarEstadoServico(Servico solicitacao, int estado) {
        solicitacao.setEstado(estado);
        ServicoDAO servicoDAO = new ServicoDAO();
        servicoDAO.salvar(solicitacao);        
        solicitacoes.remove(solicitacao);
    }
    
    /*
    * Histórico de serviços.
    */
    
    public String historicos() {
        if (!carregarContratanteLogado() && !carregarDiaristaLogado()) {
            adicionarLog(ACAO_LOG_HISTORICO, 0, "Tentativa de acessar históricos com usuário deslogado");
            return "login?faces-redirect=true";
        }
        carregarHistorico(new ServicoDAO());
        limparCamposPesquisa();
        return ("historico?faces-redirect=true");
    }
    
    private void carregarHistorico(ServicoDAO servicoDAO) {
        if (diarista != null && contratante == null) {
            servicos         = servicoDAO.buscarHistoricos(0, diarista.getId());
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_HISTORICO, diarista.getId(), "Histórico "
                                + "carregados do diarista: " + diarista.toString() 
                                + " histórico: " + servicos.toString());
            
            return;
        }
        if (contratante != null & diarista == null) {
            servicos         = servicoDAO.buscarHistoricos(contratante.getId(), 0);
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_HISTORICO, contratante.getId(), "Histórico "
                                + "carregados do contratante: " + contratante.toString() 
                                + " histórico: " + servicos.toString());
        }
    }
    
    public void buscarHistoricos() {
        carregarHistorico(new ServicoDAO(), data, turno);
    }
    
    private void carregarHistorico(ServicoDAO servicoDAO, Date data, String turno) {
        if (diarista != null && contratante == null) {
            servicos         = servicoDAO.buscarHistoricos(0, diarista.getId(), data, turno);
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_HISTORICO, diarista.getId(),
                    "Buscar históricos do diarista: " + diarista.toString() 
                    + " com os parâmetros: Data:" + data + ", Turno: " + turno
                    + " Resultado: " + servicos.toString());
            
            return;
        }
        if (contratante != null & diarista == null) {
            servicos         = servicoDAO.buscarHistoricos(contratante.getId(), 0, data, turno);
            mostrarResultado = true;
            
            adicionarLog(ACAO_LOG_HISTORICO, contratante.getId(),
                    "Buscar históricos do contratante: " + contratante.toString() 
                    + " com os parâmetros: Data:" + data + ", Turno: " + turno
                    + " Resultado: " + servicos.toString());
        }
    }
    
    /*
    * Usuário logado.
    */

    private boolean carregarContratanteLogado() {
        FacesContext context = FacesContext.getCurrentInstance();
        Object usuario = context.getExternalContext().getSessionMap().get("usuarioLogado");
        if (usuario instanceof Contratante) {
            contratante = (Contratante) usuario;
            return true;
        }
        return false;
    }

    private boolean carregarDiaristaLogado() {
        FacesContext context = FacesContext.getCurrentInstance();
        Object usuario = context.getExternalContext().getSessionMap().get("usuarioLogado");
        if (usuario instanceof Diarista) {
            diarista = (Diarista) usuario;
            return true;
        }
        return false;
    }
    
    private void limparCamposPesquisa() {
        turno = "";
        data  = new Date();
    }
    
    /*
    * Adicionar log.
    */
    
    protected void adicionarLog(String acao, String dados) {
        long id = 0;
        if (carregarDiaristaLogado()) {
            id = diarista.getId();
        }
        if (carregarContratanteLogado()) {
            id = contratante.getId();
        }
        Log log = new Log(acao, new Date(), id, dados);
        ClienteLogs cliente = new ClienteLogs();
        cliente.adicionarLog(log);
    }
    
    protected void adicionarLog(String acao, long idUsuario, String dados) {
        Log log = new Log(acao, new Date(), idUsuario, dados);
        ClienteLogs cliente = new ClienteLogs();
        cliente.adicionarLog(log);
    }

    /*
    * Getters and Setters.
    */

    public Diarista getDiarista() {
        return diarista;
    }

    public void setDiarista(Diarista diarista) {
        this.diarista = diarista;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }
    
    public List<Servico> getSolicitacoes() {
        return solicitacoes;
    }

    public void setSolicitacoes(List<Servico> solicitacoes) {
        this.solicitacoes = solicitacoes;
    }
    
    public Boolean getMostrarResultado() {
        return mostrarResultado;
    }

    public void setMostrarResultado(Boolean mostrarResultado) {
        this.mostrarResultado = mostrarResultado;
    }
    
    public Contratante getContratante() {
        return contratante;
    }

    public void setContratante(Contratante contratante) {
        this.contratante = contratante;
    }
    
    public List<Diarista> getDiaristas() {
        return diaristas;
    }

    public void setDiaristas(List<Diarista> diaristas) {
        this.diaristas = diaristas;
    }
    
    public String getTurnoFormatado() {
        if (turno == null) {
            return "";
        }
        switch (turno) {
            case "T" : return "Tarde";
            case "M" : return "Manhã";
            case "D" : return "Diária";
            default  : return "Sem turno";
        }
    }
    
    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }
    
    public String getDataFormatada() {
        if (data == null) {
            data = new Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(data);
    }
    
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
