package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Diarista implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    // Usuário
    private String usuario;
    private String senha;
    
    private String nome;
    private String email;
    private String telefone;
    private String turno;
    
    @Temporal(TemporalType.DATE)
    private Date data;
    
    /* 
    * Construtores
    */
    
    public Diarista() {}
    
    public Diarista(String usuario, String senha, String nome, String email, String telefone, String turno, Date data) {
        this.usuario = usuario;
        this.senha = senha;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.turno = turno;
        this.data = data;
    }

    public Diarista(Long id, String usuario, String senha, String nome, String email, String telefone, String turno, Date data) {
        this.id = id;
        this.usuario = usuario;
        this.senha = senha;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.turno = turno;
        this.data = data;
    }
    
    @Override
    public String toString() {
        return "Diarista{" + "id=" + id + ", usuario=" + usuario + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone + ", turno=" + turno + ", data=" + data + '}';
    }
    

//    public List<Servico> getServicos() {
//        return servicos;
//    }
//
//    public void setServicos(List<Servico> servicos) {
//        this.servicos = servicos;
//    }
//
    
//    public List<Servico> getSolicitados() {
//        List<Servico> solicitados = new LinkedList<>();
//        
//        /*
//        for (Servico servico : servicos) {
//        if (servico.getEstado().equals(Servico.Estado.SOLICITADO)) {
//        solicitados.add(servico);
//        }
//        }
//        */
//        servicos.stream().filter((servico) -> (servico.getEstado().equals(Servico.Estado.SOLICITADO))).forEach((servico) -> {
//            solicitados.add(servico);
//        });
//        return solicitados;
//    }
//    
//    public List<Servico> getAgendados() {
//        List<Servico> agendados = new LinkedList<>();
//                
//        servicos.stream().filter((servico) -> (servico.getEstado().equals(Servico.Estado.AGENDADO))).forEach((servico) -> {
//            agendados.add(servico);
//        });
//        return agendados;
//    }

    /* 
    * Getters and Setters
    */
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTurnoFormatado() {
        switch (turno) {
            case "T" : return "Tarde";
            case "M" : return "Manhã";
            case "D" : return "Diária";
            default  : return "Sem turno";
        }
    }
    
    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public String getDataFormatada() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(data);
    }


    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
