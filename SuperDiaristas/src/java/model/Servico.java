
package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Servico implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final Integer ESTADO_SOLICITADO = 1;
    public static final Integer ESTADO_AGENDADO   = 2;
    public static final Integer ESTADO_CANCELADO  = 3;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToOne
    private Diarista diarista;
    @OneToOne
    private Contratante contratante;
    private Integer estado;
    private String turno;
    private Date data;

    /* 
    * Construtores
    */
    
    public Servico() {}
    
    public Servico(Contratante contratante, Diarista diarista, Integer estado, String turno, Date data) {
        this.diarista = diarista;
        this.contratante = contratante;
        this.estado = estado;
        this.turno = turno;
        this.data = data;
    }
    
    public Servico(Long id, Diarista diarista, Contratante contratante, Integer estado, String turno, Date data) {
        this.id = id;
        this.diarista = diarista;
        this.contratante = contratante;
        this.estado = estado;
        this.turno = turno;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Servico{" + "id=" + id + ", diarista=" + diarista + ", contratante=" + contratante + ", estado=" + estado + ", turno=" + turno + ", data=" + data + '}';
    }
    
    /* 
    * Getters and Setters
    */

    public String getTurnoFormatado() {
        if (turno == null) {
            return "";
        }
        switch (turno) {
            case "T" : return "Tarde";
            case "M" : return "Manhã";
            case "D" : return "Diária";
            default  : return "Sem turno";
        }
    }
    
    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getDataFormatada() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(data);
    }
    
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
    public Diarista getDiarista() {
        return diarista;
    }

    public void setDiarista(Diarista diarista) {
        this.diarista = diarista;
    }

    public Contratante getContratante() {
        return contratante;
    }

    public void setContratante(Contratante contratante) {
        this.contratante = contratante;
    }

    public String getEstadoFormatado() {
        if (estado.equals(Servico.ESTADO_AGENDADO)) {
            return "Agendado";
        } else if (estado.equals(Servico.ESTADO_CANCELADO)) {
            return "Cancelado";
        } else if (estado.equals(Servico.ESTADO_SOLICITADO)) {
            return "Solicitado";
        }
        return "Sem estado";
    }
    
    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
