package model;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Log {
    
    private String acao;
    private Date data;
    private Long idUsuario;
    private String dados;

    public Log() {}

    public Log(String acao, Date data, Long idUsuario, String dados) {
        this.acao = acao;
        this.data = data;
        this.idUsuario = idUsuario;
        this.dados = dados;
    }
    
    @Override
    public String toString() {
        return "Log{" + "acao=" + acao + ", data=" + data + ", idUsuario=" + idUsuario + ", dados=" + dados + '}';
    }

    public String getDados() {
        return dados;
    }

    public void setDados(String dados) {
        this.dados = dados;
    }
    
    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }
}
