/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.Gson;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.Log;

/**
 * Jersey REST client generated for REST resource:LogsRecursos [logs]<br>
 * USAGE:
 * <pre>
 *        ClienteLogs client = new ClienteLogs();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Yago
 */
public class ClienteLogs {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/Logs/webresources";

    public ClienteLogs() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("logs");
    }

    public String adicionarLog(Log log) throws ClientErrorException {
        WebTarget resource = webTarget;
        Gson gson = new Gson();
        String json = gson.toJson(log);
        System.out.println(json);
        
        Response response =  resource.request().post(Entity.entity(json, MediaType.APPLICATION_JSON));
        System.out.println(response.getStatus());
        
        final String responseEntity = response.readEntity(String.class);
        System.out.println(responseEntity);
        return(responseEntity);
    }

    public <T> T getLogs(GenericType<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(MediaType.APPLICATION_JSON).get(responseType);
    }

    public void close() {
        client.close();
    }
}
